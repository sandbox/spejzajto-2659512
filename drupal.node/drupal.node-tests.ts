/// <reference path="../drupal.core/drupal.core.d.ts"/>
/// <reference path="drupal.node.d.ts"/>

(function (
    Drupal: drupal.IDrupalStatic
) {

    'use strict';

    var nodePreviewModal: string = Drupal.theme.nodePreviewModal();

}(Drupal));
