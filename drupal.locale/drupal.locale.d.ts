// Type definitions for Drupal-8.0.x Locale
// Project: https://www.drupal.org/project/drupal
// Definitions by: Andor Dávid <https://github.com/Sweetchuck>
// Definitions: https://github.com/borisyankov/DefinitelyTyped

/// <reference path="../drupal.core/drupal.core.d.ts"/>

declare module drupal {

    export module Core {

        export interface ITheme {

            localeTranslateChangedMarker: () => string;

            localeTranslateChangedWarning: () => string;

        }

        export interface IBehaviors {

            localeTranslateDirty?: IBehavior;

            hideUpdateInformation?: IBehavior;

            importLanguageCodeSelector?: IBehavior;

            localeDatepicker?: IBehavior;

        }

    }

}
