/// <reference path="drupal.core.d.ts"/>
/// <reference path="drupal.core-active_links.d.ts"/>

(function (
    Drupal: drupal.IDrupalStatic
) {

    'use strict';

    var behavior: drupal.Core.IBehavior;

    behavior = Drupal.behaviors.activeLinks;

}(Drupal));
