/// <reference path="drupal.core.d.ts"/>
/// <reference path="drupal.core-ajax.d.ts"/>

(function (
    Drupal: drupal.IDrupalStatic
) {

    'use strict';

    var behavior: drupal.Core.IBehavior;

    behavior = Drupal.behaviors.AJAX;

}(Drupal));
