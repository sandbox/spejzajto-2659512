/// <reference path="../drupal.core/drupal.core.d.ts"/>
/// <reference path="drupal.menu_ui.d.ts"/>

(function (
    Drupal: drupal.IDrupalStatic
) {

    'use strict';

    Drupal.menuUiUpdateParentList();

}(Drupal));
