/// <reference path="../drupal.core/drupal.core.d.ts"/>
/// <reference path="drupal.quickedit.d.ts"/>

(function (
    Drupal: drupal.IDrupalStatic
) {

    'use strict';

    var behavior: drupal.Core.IBehavior;

    behavior = Drupal.behaviors.quickedit;

}(Drupal));
